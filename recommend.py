import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.neighbors import NearestNeighbors
from sklearn.decomposition import PCA
import pickle
import pymongo

myclient = pymongo.MongoClient("mongodb://rhythm-admin:rhythm-admin@rhythmdb-shard-00-00-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-01-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-02-n8ap6.gcp.mongodb.net:27017/test?ssl=true&replicaSet=RhythmDB-shard-0&authSource=admin")
mydb = myclient["test"]
mycol = mydb["songs"]

songs = list(mycol.find())

ds = pd.DataFrame(songs)
#ds = pd.read_csv('song_data.csv')

acousticness = ds['acousticness']
danceability = ds['danceability']
energy = ds['energy']
liveness = ds['liveness']
loudness = ds['loudness']
speechiness = ds['speechiness']
tempo = ds['tempo']
playlist = ds['playlist']
genre = playlist.str.get_dummies()

X = pd.concat([acousticness, danceability, energy, genre], axis=1)

#print(X)

scaled = StandardScaler()
X = scaled.fit_transform(X)

# pca = PCA(n_components=2)
# X = pca.fit_transform(X)

recommendations = NearestNeighbors(n_neighbors=11, algorithm='ball_tree').fit(X)
song_indices = recommendations.kneighbors(X)[1]

pickle.dump(song_indices,open("model.pkl","wb"));
print(song_indices)

# def get_index(x):
#     return ds[ds['song_name']==x].index.tolist()[0]

# def recommend_me(song):
#     print('Here are 5 songs similar to', song, ':' '\n')
#     index = get_index(song)
#     for i in song_indices[index][1:]:
#             print(ds.iloc[i]['song_name'], '\n')

# recommend_me("Highway to Hell")