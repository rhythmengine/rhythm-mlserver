import pickle
import pymongo
import pandas as pd
from surprise import Reader, Dataset, evaluate, KNNBasic, SVD

myclient = pymongo.MongoClient("mongodb://rhythm-admin:rhythm-admin@rhythmdb-shard-00-00-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-01-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-02-n8ap6.gcp.mongodb.net:27017/test?ssl=true&replicaSet=RhythmDB-shard-0&authSource=admin")
mydb = myclient["test"]
mycol = mydb["ratings"]

ratings = list(mycol.find())
#songs = pd.read_csv('songs.csv')
#songs = [1,2,3,4,5,6]
#model = pickle.load(open("model.pkl","rb"), encoding='latin1')

ds = pd.DataFrame(ratings)
rating = ds.drop('_id', axis=1)
rating = rating.drop('__v', axis=1)
rating = rating.drop('createdAt', axis=1)
rating = rating.drop('updatedAt', axis=1)
ratings_dict = {'userid': list(rating.userId),
				'itemid': list(rating.songId),
                'rating': list(rating.rating)}
print(rating.userId[3])
df = pd.DataFrame(ratings_dict)
reader = Reader(rating_scale=(0,5))
data = Dataset.load_from_df(df[['userid', 'itemid', 'rating']], reader=reader)
sim_options = {
    'name': 'cosine',
    'user_based': True
}
algo = KNNBasic(sim_options=sim_options)

trainset = data.build_full_trainset()
algo.fit(trainset)

pickle.dump(algo,open("model1.pkl","wb"));

# songs = ds.songId.unique()

# predicted_ratings = pd.DataFrame(columns=('userId', 'songId', 'rating'))

# for x in songs:
# 	y = algo.predict(1, x, r_ui=None, verbose=False)
# 	y_new = list(y)
# 	new_row = pd.DataFrame({'userId':y_new[0], 'songId':y_new[1], 'rating':y_new[3]}, index =[0])
# 	predicted_ratings = pd.concat([new_row, predicted_ratings]).reset_index(drop = True)


# user_ratings = rating.loc[rating['userId'] == 1]

# common = set(predicted_ratings['songId']).intersection(set(user_ratings['songId']))
# print(common)

# for x in common:
# 	predicted_ratings = predicted_ratings[predicted_ratings.songId != x]

# print(predicted_ratings.sort_values(['rating'], ascending=False))