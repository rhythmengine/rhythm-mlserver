import pickle
import flask
from flask import request
import pandas as pd
import pymongo

myclient = pymongo.MongoClient("mongodb://rhythm-admin:rhythm-admin@rhythmdb-shard-00-00-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-01-n8ap6.gcp.mongodb.net:27017,rhythmdb-shard-00-02-n8ap6.gcp.mongodb.net:27017/test?ssl=true&replicaSet=RhythmDB-shard-0&authSource=admin")
mydb = myclient["test"]
mycol = mydb["songs"]

ratingscol = mydb["ratings"]
ratings = list(ratingscol.find())

songs = list(mycol.find())

ds = pd.DataFrame(songs)

app = flask.Flask(__name__)
#loading my model
model = pickle.load(open("model.pkl","rb"), encoding='latin1')
algo = pickle.load(open("model1.pkl","rb"), encoding='latin1')

#defining a route for only post requests
@app.route('/similarsongs/<song_id>', methods=['GET'])
def similarsongs(song_id):
	song = int(song_id)
	print(song)
	recommendation = []
	index = ds[ds['id']==song].index.tolist()[0]
	for i in model[index][1:]:
		recommendation.append(ds.iloc[i]['id'])
	recommendation = map(int, recommendation)
	print(recommendation)
	output = list(recommendation)
	return flask.jsonify(output)


@app.route('/madeforyou/<user_id>', methods=['GET'])
def madeforyou(user_id):
	user_id = int(user_id)

	ds = pd.DataFrame(ratings)
	rating = ds.drop('_id', axis=1)
	rating = rating.drop('__v', axis=1)
	rating = rating.drop('createdAt', axis=1)
	rating = rating.drop('updatedAt', axis=1)
	ratings_dict = {'userid': list(rating.userId),
					'itemid': list(rating.songId),
	                'rating': list(rating.rating)}

	songs = ds.songId.unique()

	predicted_ratings = pd.DataFrame(columns=('userId', 'songId', 'rating'))

	for x in songs:
		y = algo.predict(user_id, x, r_ui=None, verbose=False)
		y_new = list(y)
		new_row = pd.DataFrame({'userId':y_new[0], 'songId':y_new[1], 'rating':y_new[3]}, index =[0])
		predicted_ratings = pd.concat([new_row, predicted_ratings]).reset_index(drop = True)


	user_ratings = rating.loc[rating['userId'] == user_id]

	common = set(predicted_ratings['songId']).intersection(set(user_ratings['songId']))

	for x in common:
		predicted_ratings = predicted_ratings[predicted_ratings.songId != x]
	
	predicted_ratings = predicted_ratings.loc[predicted_ratings['rating'] >= 2.7]
	print(predicted_ratings)

	final = predicted_ratings.sort_values(['rating'], ascending=False)
	output = list(final['songId'])
	return flask.jsonify(output)
